package main

import (
	"encoding/hex"
	"snow/ecies"
	"snow/keys"
	"syscall/js"
)

func key_generate(js.Value, []js.Value) interface{} {
	pv, pb := keys.Generate()

	return map[string]interface{}{
		"privateKey": pv,
		"publicKey":  pb,
	}
}

func ecies_crypt(i js.Value, v []js.Value) interface{} {
	pb := keys.DecodePublic(v[0].String())

	var toCrypt []byte
	js.CopyBytesToGo(toCrypt, v[1])
	res := ecies.Encrypt(pb, toCrypt)

	return hex.EncodeToString(res)
}

func main() {
	js.Global().Set("snow", map[string]interface{}{
		"keys": map[string]interface{}{
			"generate": js.FuncOf(key_generate),
		},
		"ecies": map[string]interface{}{
			"encrypt": js.FuncOf(ecies_crypt),
		},
	})

	select {}
}
