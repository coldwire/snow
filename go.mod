module snow

go 1.17

require (
	github.com/ecies/go v1.0.1
	github.com/fomichev/secp256k1 v0.0.0-20180413221153-00116ff8c62f
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)

require (
	github.com/pkg/errors v0.8.1 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
