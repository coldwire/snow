package keys

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/hex"
	"log"
	"math/big"

	"snow/utils"

	"github.com/fomichev/secp256k1"
)

var (
	secp256k1N, _ = new(big.Int).SetString("fffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141", 16)
)

func Generate() (string, string) {
	pvkey, err := ecdsa.GenerateKey(secp256k1.SECP256K1(), rand.Reader)
	if err != nil {
		panic(err)
	}

	pk := FromECDSA(pvkey)
	pv := FromECDSAPub(&pvkey.PublicKey)

	return pk, pv
}

// Convert private key to string
func FromECDSA(priv *ecdsa.PrivateKey) string {
	if priv == nil {
		return ""
	}

	b := utils.PaddedBigBytes(priv.D, priv.Params().BitSize/8)
	return hex.EncodeToString(b)
}

// Convert public key to bytes
func FromECDSAPub(pub *ecdsa.PublicKey) string {
	if pub == nil || pub.X == nil || pub.Y == nil {
		return ""
	}

	b := elliptic.Marshal(secp256k1.SECP256K1(), pub.X, pub.Y)
	return hex.EncodeToString(b)
}

func DecodePublic(pb string) *ecdsa.PublicKey {
	s, err := hex.DecodeString(pb)
	if err != nil {
		log.Println(err)
	}

	x, y := elliptic.Unmarshal(secp256k1.SECP256K1(), s)
	if x == nil {
		return nil
	}
	return &ecdsa.PublicKey{Curve: secp256k1.SECP256K1(), X: x, Y: y}
}

func DecodePrivate(pv string) *ecdsa.PrivateKey {
	s, err := hex.DecodeString(pv)
	if err != nil {
		log.Println(err)
	}

	priv := new(ecdsa.PrivateKey)
	priv.PublicKey.Curve = secp256k1.SECP256K1()

	priv.D = new(big.Int).SetBytes(s)

	// The priv.D must < N
	if priv.D.Cmp(secp256k1N) >= 0 {
		log.Println("invalid private key, >=N")
	}

	if priv.D.Sign() <= 0 {
		log.Println("invalid private key, zero or negative")
	}

	priv.PublicKey.X, priv.PublicKey.Y = priv.PublicKey.Curve.ScalarBaseMult(s)
	if priv.PublicKey.X == nil {
		log.Println("invalid private key")
	}

	return priv
}
