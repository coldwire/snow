package main

import (
	"crypto/ecdsa"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"math/big"
)

type Signature struct {
	R *big.Int
	S *big.Int
}

func Sign(pvkey *ecdsa.PrivateKey, data []byte) (string, error) {
	r, s, err := ecdsa.Sign(rand.Reader, pvkey, data)
	if err != nil {
		return "", err
	}

	signature := &Signature{
		R: r,
		S: s,
	}

	// marshal to json
	signature_json, err := json.Marshal(signature)
	if err != nil {
		return "nil", err
	}

	sig := hex.EncodeToString(signature_json)

	return sig, nil
}

func Verify(pbkey *ecdsa.PublicKey, data []byte, sig string) (bool, error) {
	signature_json, err := hex.DecodeString(sig)
	if err != nil {
		return false, err
	}

	var signature Signature
	err = json.Unmarshal(signature_json, &signature)
	if err != nil {
		return false, err
	}

	isValid := ecdsa.Verify(pbkey, data, signature.R, signature.S)
	return isValid, nil
}
