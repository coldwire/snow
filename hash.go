package main

import (
	"io"
	"log"

	"golang.org/x/crypto/sha3"
)

func Hash(data []byte) []byte {
	algo := sha3.New256()
	algo.Write(data)
	return algo.Sum(nil)
}

func HashFile(file io.Reader) []byte {
	hash := sha3.New256()

	_, err := io.Copy(hash, file)
	if err != nil {
		log.Fatal(err)
	}

	return hash.Sum(nil)
}
