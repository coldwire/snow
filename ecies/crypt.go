package ecies

import (
	"crypto/ecdsa"
	"log"

	ecies "github.com/ecies/go"
)

func Encrypt(key *ecdsa.PublicKey, content []byte) []byte {
	k := importECDSAPublic(key)

	cipher, err := ecies.Encrypt(k, content)
	if err != nil {
		log.Println(err)
	}

	return cipher
}

func Decrypt(key *ecdsa.PrivateKey, content []byte) []byte {
	k := importECDSAPrivate(key)

	result, err := ecies.Decrypt(k, content)
	if err != nil {
		log.Println(err)
	}

	return result
}
