package ecies

import (
	"crypto/ecdsa"

	ecies "github.com/ecies/go"
)

func importECDSAPublic(pb *ecdsa.PublicKey) *ecies.PublicKey {
	return &ecies.PublicKey{
		X:     pb.X,
		Y:     pb.Y,
		Curve: pb.Curve,
	}
}

func importECDSAPrivate(pv *ecdsa.PrivateKey) *ecies.PrivateKey {
	pb := importECDSAPublic(&pv.PublicKey)
	return &ecies.PrivateKey{PublicKey: pb, D: pv.D}
}
